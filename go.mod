module gitlab.com/omnifi/automaatio/cryptography

go 1.21.1

require (
    golang.org/x/crypto v0.14.0
    gitlab.com/omnifi/automaatio/io v0.0.0
)

require (
	golang.org/x/sys v0.13.0 // indirect
)

replace gitlab.com/omnifi/automaatio/io => gitlab.com/omnifi/automaatio/io.git v0.0.0
