package signatures

import (
	"crypto/ed25519"
	"testing"
)

// TestED25519KeyGeneration tests the creation of ED25519 key pairs.
func TestED25519KeyGeneration(t *testing.T) {
	publicKey, privateKey, err := GenerateEd25519Keys()

	if err != nil {
		t.Fatal(err)
	}

	message := []byte("The quick brown fox jumps over the lazy dog")

	signature, err := privateKey.Sign(nil, message, &ed25519.Options{
		Context: "Example_ed25519ctx",
	})

	if err != nil {
		t.Fatal(err)
	}

	if err := ed25519.VerifyWithOptions(publicKey, message, signature, &ed25519.Options{
		Context: "Example_ed25519ctx",
	}); err != nil {
		t.Fatal("invalid signature")
	}
}
