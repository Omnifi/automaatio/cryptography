package signatures

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
)

// Generates Ed25519 keys for cryptography.
func GenerateEd25519Keys() (publicKey ed25519.PublicKey, privateKeyString ed25519.PrivateKey, err error) {
	publicKey, privateKey, err := ed25519.GenerateKey(rand.Reader)

	if err != nil {
		return
	}

	message := []byte("The quick brown fox jumps over the lazy dog")

	signature, err := privateKey.Sign(nil, message, &ed25519.Options{
		Context: "Example_ed25519ctx",
	})

	if err != nil {
		return
	}

	if err = ed25519.VerifyWithOptions(publicKey, message, signature, &ed25519.Options{
		Context: "Example_ed25519ctx",
	}); err != nil {
		return
	}

	return publicKey, privateKey, err
}

// GenerateECDSAKeys generates ECDSA public and private key pair with given size.
func GenerateECDSAKeys(bitSize int) (ecdsaPrivateKey *ecdsa.PrivateKey, err error) {
	// generate private key
	var privateKey *ecdsa.PrivateKey
	if privateKey, err = ecdsa.GenerateKey(curveFromLength(bitSize), rand.Reader); err != nil {
		return
	}

	return privateKey, err
}

// Returns an elliptic curve from a given length for use with ECDSA key generation.
func curveFromLength(length int) elliptic.Curve {
	switch length {
	case 224:
		return elliptic.P224()
	case 256:
		return elliptic.P256()
	case 348:
		return elliptic.P384()
	case 521:
		return elliptic.P521()
	}
	return elliptic.P384()
}
