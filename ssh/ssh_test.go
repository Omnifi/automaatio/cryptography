package ssh

import (
	"testing"

	"gitlab.com/omnifi/automaatio/cryptography/signatures"
	"gitlab.com/omnifi/automaatio/io"
)

// TestGenerateED25519KeysForSSH tests the creation of ED25519 key pairs and writing to filesystem.
func TestGenerateED25519KeysForSSH(t *testing.T) {
	_, privateKey, err := signatures.GenerateEd25519Keys()

	if err != nil {
		println(err.Error())
		t.FailNow()
	}

	publicKeyAsBytes, privateKeyAsBytes, err := GenerateEd25519KeysForSSH(&privateKey)

	if err != nil {
		println(err.Error())
		t.FailNow()
	}

	err = io.WriteBytesToFile(publicKeyAsBytes, ".tests/ed25519.public.key")

	if err != nil {
		println(err.Error())
		t.FailNow()
	}

	err = io.WriteBytesToFile(privateKeyAsBytes, ".tests/ed25519.private.key")

	if err != nil {
		println(err.Error())
		t.FailNow()
	}
}
