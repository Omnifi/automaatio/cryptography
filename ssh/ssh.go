package ssh

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/x509"
	"encoding/pem"
	"math/rand"

	"golang.org/x/crypto/ssh"
)

// GenerateECDSAKeys generates ECDSA public and private key pair with given size for SSH.
func GenerateECDSAKeysForSSH(ecdsaPrivateKey *ecdsa.PrivateKey) (publicKeyString string, privateKeyString string, err error) {
	// encode public key
	var (
		bytes     []byte
		publicKey ssh.PublicKey
	)

	if publicKey, err = ssh.NewPublicKey(ecdsaPrivateKey.Public()); err != nil {
		return
	}

	publicKeyAsBytes := ssh.MarshalAuthorizedKey(publicKey)

	// encode private key
	if bytes, err = x509.MarshalECPrivateKey(ecdsaPrivateKey); err != nil {
		return
	}

	privateKeyAsBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "ECDSA PRIVATE KEY",
		Bytes: bytes,
	})

	return string(publicKeyAsBytes), string(privateKeyAsBytes), nil
}

// GenerateEd25519KeysForSSH generates ED25519 public and private key pair for use in SSH.
func GenerateEd25519KeysForSSH(privateKey *ed25519.PrivateKey) (publicKeyAsBytes []byte, privateKeyAsBytes []byte, err error) {
	// encode public key
	var (
		bytes     []byte
		publicKey ssh.PublicKey
	)

	if publicKey, err = ssh.NewPublicKey(privateKey.Public()); err != nil {
		return
	}

	publicKeyAsBytes = ssh.MarshalAuthorizedKey(publicKey)

	// encode private key
	// x509.MarshalPKCS8PrivateKey(*ed25519PrivateKey) doesn't currently work.
	// a custom function has been created as a temporary measure to handle this.
	if bytes, err = marshalEd25519PrivateKey(*privateKey); err != nil {
		return
	}

	privateKeyAsBytes = pem.EncodeToMemory(&pem.Block{
		Type:  "OPENSSH PRIVATE KEY",
		Bytes: bytes,
	})

	_, err = ssh.ParsePrivateKey(privateKeyAsBytes)

	return
}

// Marshals an ED25519 private key for use in SSH.
func marshalEd25519PrivateKey(privateKey ed25519.PrivateKey) ([]byte, error) {
	// Add key header (followed by a null byte)
	bytes := append([]byte("openssh-key-v1"), 0)

	var w struct {
		CipherName   string
		KdfName      string
		KdfOpts      string
		NumKeys      uint32
		PubKey       []byte
		PrivKeyBlock []byte
	}

	// PK1 private key fields
	pk1 := struct {
		Check1  uint32
		Check2  uint32
		Keytype string
		Pub     []byte
		Priv    []byte
		Comment string
		Pad     []byte `ssh:"rest"`
	}{}

	// Set check integers
	checkIntegers := rand.Uint32()
	pk1.Check1 = checkIntegers
	pk1.Check2 = checkIntegers

	// Set our key type
	pk1.Keytype = ssh.KeyAlgoED25519

	// Add the pubkey to the optionally-encrypted block
	publicKey, success := privateKey.Public().(ed25519.PublicKey)

	if !success {
		//fmt.Fprintln(os.Stderr, "ed25519.PublicKey type assertion failed on an ed25519 public key. This should never ever happen.")
		return nil, nil
	}

	publicKeyAsBytes := []byte(publicKey)
	pk1.Pub = publicKeyAsBytes

	// Add our private key
	pk1.Priv = []byte(privateKey)

	// Might be useful to put something in here at some point
	pk1.Comment = ""

	// Add some padding to match the encryption block size within PrivKeyBlock (without Pad field)
	// 8 doesn't match the documentation, but that's what ssh-keygen uses for unencrypted keys. *shrug*
	blockSize := 8
	blockLength := len(ssh.Marshal(pk1))
	paddingLength := (blockSize - (blockLength % blockSize)) % blockSize
	pk1.Pad = make([]byte, paddingLength)

	// Padding is a sequence of bytes like: 1, 2, 3...
	for index := 0; index < paddingLength; index++ {
		pk1.Pad[index] = byte(index + 1)
	}

	// Generate the pubkey prefix "\0\0\0\nssh-ed25519\0\0\0 "
	prefix := []byte{0x0, 0x0, 0x0, 0x0b}
	prefix = append(prefix, []byte(ssh.KeyAlgoED25519)...)
	prefix = append(prefix, []byte{0x0, 0x0, 0x0, 0x20}...)

	// Only going to support unencrypted keys for now
	w.CipherName = "none"
	w.KdfName = "none"
	w.KdfOpts = ""
	w.NumKeys = 1
	w.PubKey = append(prefix, publicKeyAsBytes...)
	w.PrivKeyBlock = ssh.Marshal(pk1)

	bytes = append(bytes, ssh.Marshal(w)...)

	return bytes, nil
}
